
public class CriaFigura {

    public static void main(String[] args) {

        FiguraGeometrica[] listaFiguras = {
                                           new Quadrado(4),
                                           new Triangulo(3, 4),
                                           new Quadrado(6.5),
                                           new Quadrado(10.00),
                                           new Circulo(2),
                                           new Triangulo(6.5, 10),
                                           new Trapezio(4.00, 8.00, 20.00)
        };
        
        for (FiguraGeometrica i : listaFiguras) {
            System.out.println();
            System.out.println("Area do(a) " + i.getNomeFigura() + ": " + i.getArea());
        }
        
        int qtdQuadrado = 0;
        for (FiguraGeometrica i : listaFiguras) {
            if (i.getNomeFigura() == "Quadrado") {
                qtdQuadrado++;
            }
        }
          
        System.out.println();
        System.out.println("Quantidade de Quadrados: " + qtdQuadrado );

    }
}

public class Trapezio extends FiguraGeometrica {

    private double baseSuperior;
    private double baseInferior;
    private double altura;
    
    public Trapezio(double baseSuperior, double baseInferior, double altura) {
        super();
        this.baseSuperior = baseSuperior;
        this.baseInferior = baseInferior;
        this.altura = altura;
        
        
    }

    public double getBaseSuperior() {
        return baseSuperior;
    }

    public void setBaseSuperior(double baseSuperior) {
        this.baseSuperior = baseSuperior;
    }

    public double getBaseInferior() {
        return baseInferior;
    }

    public void setBaseInferior(double baseInferior) {
        this.baseInferior = baseInferior;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltrua(double altura) {
        this.altura = altura;
    }
    
    public double getArea() {
        return altura *(baseInferior + baseSuperior)/2;
    }
    public String getNomeFigura() {
        return "Trapezio";
    }
    
}

public class CriaDesenho {
    public static void main(String[] args) {
        System.out.println(" Area do trapezio =================================");
        System.out.println();
        Trapezio[] trapezio = {

                               new Trapezio(4.00, 8.00, 2.00),
                               new Trapezio(1.50, 3.00, 1.00),
                               new Trapezio(0.10, 0.80, 0.50),
                               new Trapezio(2.00, 3.00, 1.00)
        };
        for (Trapezio i : trapezio) {
            System.out.println(" area: " + i.getArea());
        }
        System.out.println();
        System.out.println(" Area do triangulo ================================");
        System.out.println();
        Triangulo[] triangulo = {

                                 new Triangulo(10.00, 20.00),
                                 new Triangulo(5.00, 10.00),
                                 new Triangulo(10.00, 8.00),
                                 new Triangulo(3.00, 6.00)
        };
        for (Triangulo i : triangulo) {
            System.out.println(" area: " + i.getArea());
        }
        System.out.println();
        System.out.println(" Area do Quadrado =================================");
        System.out.println();
        Quadrado [] quadrado = {
                                
                                new Quadrado(2.00 ),
                                new Quadrado(1.00 ),
                                new Quadrado(5.00 ),
                                new Quadrado(3.00 )
        };
        
        for (Quadrado i : quadrado) {
            System.out.println(" area: " + i.getArea());
        }
        System.out.println();
        System.out.println(" Area do Circulo =================================");
        System.out.println();
        Circulo[] circulo = {
                             
                             new Circulo(1.5),
                             new Circulo(2.00)
        };
        for (Circulo i : circulo) {
            System.out.println(" area: " + i.getArea() );
        }
    }

}
